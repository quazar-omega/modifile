import 'package:flutter/material.dart';
import 'package:modifile/models/file_list.dart';
import 'package:provider/provider.dart';
import 'pages/home/home.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.amber[50],
          colorScheme: ColorScheme.fromSwatch().copyWith(
              primary: Colors.amber[50], secondary: Colors.amber[300]),
          //textTheme: TextTheme(bodyText1: Te),
          appBarTheme: AppBarTheme(
              backgroundColor: Colors.amber[50],
              foregroundColor: Colors.brown[600],
              elevation: 0),
          navigationBarTheme: NavigationBarThemeData(
              backgroundColor: Colors.amber[100],
              indicatorColor: Colors.amber[300])),
      home: ChangeNotifierProvider(
          create: (context) => FileList(), child: const Home()),
    );
  }
}
