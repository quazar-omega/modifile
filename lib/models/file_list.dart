import 'package:flutter/foundation.dart';

class FileList extends ChangeNotifier {
  final List<String> _paths = [];

  List<String> get paths => _paths;

  void add(String path) {
    _paths.add(path);
    notifyListeners();
  }

  void remove(String path) {
    _paths.remove(path);
    notifyListeners();
  }
}
