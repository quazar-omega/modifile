import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:settings_ui/settings_ui.dart';

class Settings extends StatelessWidget {
  const Settings({Key? key}) : super(key: key);

  final String _authorUrl = "https://gitlab.com/quazar-omega";
  final String _sourceUrl = "https://gitlab.com/quazar-omega/modifile";

  void openUrl(url) async {
    if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url), mode: LaunchMode.externalApplication);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Settings")),
      body: SettingsList(
        sections: [
          SettingsSection(
            title: const Text('About'),
            tiles: <SettingsTile>[
              SettingsTile(
                leading: const Icon(Icons.info),
                title: const Text("Version"),
                value: const Text("0.1"),
              ),
              SettingsTile(
                leading: const Icon(Icons.person),
                title: const Text("Author"),
                value: const Text("Fabrizio Volonté"),
                onPressed: (context) => openUrl(_authorUrl),
              ),
              SettingsTile(
                leading: const Icon(Icons.code),
                title: const Text("Source code"),
                value: Text(_sourceUrl),
                onPressed: (context) => openUrl(_sourceUrl),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
