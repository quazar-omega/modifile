import 'package:ffmpeg_kit_flutter/ffprobe_kit.dart';
import 'package:file_picker/file_picker.dart';
import 'package:ffmpeg_kit_flutter/media_information_session.dart';
import 'package:flutter/material.dart';
import 'package:modifile/models/file_list.dart';
import 'package:provider/provider.dart';
import './convert.dart';
import './history.dart';

import '../settings.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  static String originalFilePath = "";

  static final List<Widget> _pages = <Widget>[Convert(), const History()];
  static final List<String> _popupMenuItems = ["Settings"];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (String item) {
              switch (item) {
                case "Settings":
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const Settings()));
                  break;
                default:
              }
            },
            itemBuilder: (BuildContext context) {
              return _popupMenuItems.map((item) {
                return PopupMenuItem<String>(
                  value: item,
                  child: Text(item),
                );
              }).toList();
            },
          )
        ],
        elevation: 0,
      ),
      body: _pages.elementAt(_selectedIndex),
      bottomNavigationBar: NavigationBar(
          selectedIndex: _selectedIndex,
          onDestinationSelected: (index) => setState(() {
                _selectedIndex = index;
              }),
          labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
          destinations: const [
            NavigationDestination(
              icon: Icon(Icons.swap_horiz),
              label: "Convert",
            ),
            NavigationDestination(
              icon: Icon(Icons.history),
              label: "History",
            ),
          ]),
      floatingActionButton: _selectedIndex == 0
          ? Theme(
              data: Theme.of(context).copyWith(
                  floatingActionButtonTheme:
                      const FloatingActionButtonThemeData(
                          extendedSizeConstraints:
                              BoxConstraints.tightFor(height: 60))),
              child: FloatingActionButton.extended(
                onPressed: () async {
                  var result = await FilePicker.platform.pickFiles();
                  String? path = result?.paths[0].toString();
                  /* MediaInformationSession info =
                      await FFprobeKit.getMediaInformation(path!); */
                  FileList fileList = context.read<FileList>();
                  fileList.add(path!);
                },
                icon: const Icon(Icons.note_add_outlined),
                label: const Text(
                  "Add file",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(22)),
                //backgroundColor: Colors.lightBlue[100],
                foregroundColor: Colors.black87,
              ))
          : null,
    );
  }
}
