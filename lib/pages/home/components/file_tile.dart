import 'dart:io';

import 'package:ffmpeg_kit_flutter/ffmpeg_kit.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:image/image.dart' as img;
import 'package:path/path.dart' as p;

import '../../../models/file_extension.dart';

class FileTile extends StatefulWidget {
  final String originalFilePath;

  const FileTile({
    Key? key,
    required this.originalFilePath,
  }) : super(key: key);

  @override
  State<FileTile> createState() => _FileTileState();
}

class _FileTileState extends State<FileTile> {
  String? savePath;
  String? filename;
  FileExtension dropdownValue = FileExtension.png;
  bool useFfmpeg = false;

  @override
  void initState() {
    super.initState();
    savePath = p.dirname(widget.originalFilePath);
    filename =
        "${p.basenameWithoutExtension(widget.originalFilePath)}.${dropdownValue.name}";
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
            decoration: BoxDecoration(
              color: Colors.blue[400],
              borderRadius: const BorderRadius.all(Radius.circular(12.0)),
            ),
            child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(children: [
                  ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(8)),
                      child: AspectRatio(
                          aspectRatio: 1,
                          child: Image.file(
                              height: 80,
                              fit: BoxFit.cover,
                              File(widget.originalFilePath)))),
                  Text(p.basename(widget.originalFilePath)),
                  ElevatedButton.icon(
                    icon: const Icon(Icons.save),
                    label: const Text("Choose save folder"),
                    onPressed: () async {
                      await FilePicker.platform.getDirectoryPath().then(
                          (pickedFolder) =>
                              setState((() => savePath = pickedFolder)));
                    },
                  ),
                  Row(
                    children: [
                      Checkbox(
                        checkColor: Colors.white,
                        value: useFfmpeg,
                        onChanged: (bool? value) {
                          setState(() {
                            useFfmpeg = value!;
                          });
                        },
                      ),
                      DropdownButton<FileExtension>(
                          items:
                              FileExtension.values.map((FileExtension value) {
                            return DropdownMenuItem<FileExtension>(
                              value: value,
                              child: Text(value.name),
                            );
                          }).toList(),
                          value: dropdownValue,
                          onChanged: (FileExtension? newValue) {
                            setState(() {
                              dropdownValue = newValue!;
                              filename =
                                  "${p.basenameWithoutExtension(widget.originalFilePath)}.${dropdownValue.name}";
                            });
                          },
                          elevation: 16,
                          icon: const Icon(Icons.arrow_downward)),
                    ],
                  ),
                  Text("$savePath/$filename"),
                  ElevatedButton(
                      onPressed: () {
                        if (useFfmpeg) {
                          switch (dropdownValue) {
                            case FileExtension.jpg:
                            case FileExtension.png:
                              FFmpegKit.execute(
                                  "-i ${widget.originalFilePath} $savePath/$filename");
                              break;
                            case FileExtension.webp:
                              FFmpegKit.execute(
                                  "-i ${widget.originalFilePath} $savePath/$filename");
                              break;
                            default:
                              FFmpegKit.execute(
                                  "-i ${widget.originalFilePath} $savePath/$filename");
                          }
                        } else {
                          final image = img.decodeImage(
                              File(widget.originalFilePath).readAsBytesSync());
                          List<int> encoded;
                          switch (dropdownValue) {
                            case FileExtension.jpg:
                              encoded = img.encodeJpg(image!);
                              break;
                            case FileExtension.png:
                              encoded = img.encodePng(image!);
                              break;
                            case FileExtension.webp:
                              encoded = img.encodePng(image!);
                              break;
                            default:
                              encoded = img.encodePng(image!);
                          }

                          File("$savePath/$filename").writeAsBytesSync(encoded);
                        }
                      },
                      child: const Text("Convert")),
                ]))));
  }
}
