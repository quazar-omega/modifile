import 'package:flutter/material.dart';
import 'package:modifile/models/file_list.dart';
import 'package:modifile/pages/home/components/file_tile.dart';
import 'package:provider/provider.dart';

class Convert extends StatelessWidget {
  const Convert({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<FileList>(
        builder: (context, fileList, child) => fileList.paths.isNotEmpty
            ? ListView.builder(
                itemCount: fileList.paths.length,
                itemBuilder: (context, index) =>
                    FileTile(originalFilePath: fileList.paths.elementAt(index)))
            : const Center(
                child: Text(
                  "No file added",
                  style: TextStyle(
                      fontWeight: FontWeight.w100,
                      fontSize: 40,
                      color: Colors.black38),
                ),
              ));
  }
}
