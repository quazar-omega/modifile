# modifile <sup>pre-alpha</sup>

An app to convert various file formats, be it images or videos (maybe more? ..soon™).


## Screenshots

<img width="150px" src="./display/convert.png" alt="convert">


## Mockups

<img width="150px" src="./display/mockups/convert-page-base.svg" alt="convert">

## Supported platforms

- Android
- iOS <sup>untested</sup>

### Supported with limited features
- Linux
- Windows <sup>untested</sup>
- macOS <sup>untested</sup>


## ffmpeg conversion notes

<table>
	<tr>
		<th>Input</th>
		<th>Output</th>
		<th>Command</th>
	</tr>
	<tr>
		<th colspan="3" style="text-align: center;">Images</th>
	</tr>
	<tr>
		<td>png</td>
		<td>jpg</td>
		<td><code>ffmpeg -i file.png file.jpg</code></td>
	</tr>
	<tr>
		<th colspan="3" style="text-align: center;">Videos</th>
	</tr>
	<tr>
		<td>mkv</td>
		<td>mp4</td>
		<td><code>ffmpeg -i file.mkv -c copy file.mp4</code></td>
	</tr>
</table>